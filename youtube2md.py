#!/usr/bin/env python3

import os
import sys
import json
import urllib.request
import glob
import warnings
from itertools import count
from typing import Optional, Union, Any
from collections.abc import Sequence, Iterable, Callable
from contextlib import contextmanager

from dataclasses import dataclass, field, asdict, replace


@dataclass
class Video:
    title: str
    url: str
    description: str
    created: Optional[str] = None
    md_path: Optional[str] = None
    tags: frozenset[str] = field(default_factory=frozenset)
    thumbnails: dict[str, dict[str, Union[int, str]]] = field(default_factory=dict)


@dataclass
class Config:
    root: str
    api_key: str
    channel_ids: list[str]
    subfolder_by_tags: dict[frozenset[str], str]

    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> "Config":
        subfolder_by_tags = data.pop("subfolder_by_tags")
        return cls(
            **data,
            subfolder_by_tags={
                frozenset(mapping["tags"]): mapping["folder"]
                for mapping in subfolder_by_tags
            },
        )

    @classmethod
    def load_from_file(cls, path: str) -> "Config":
        with open(path, encoding="utf-8") as f:
            return cls.from_dict(json.load(f))


def sync_from_youtube(
    youtube_videos: list[Video],
    folder: str,
    subfolder_by_tags: dict[frozenset[str], str],
) -> None:
    md_videos = import_md_subfolders(folder, subfolder_by_tags)
    new_videos, deleted_videos, video_changes = diff_videos(
        md_videos,
        list(map_youtube_tags_to_folder_tags(youtube_videos, subfolder_by_tags)),
    )
    taken_paths: set[str] = {get_path(video) for video in md_videos}
    for video in deleted_videos:
        path = get_path(video)
        os.unlink(path)
        taken_paths.remove(path)
    for old, new in video_changes:
        export_md(new, get_path(old))
    for video in new_videos:
        subfolder = subfolder_by_tags.get(frozenset(video.tags))
        if subfolder is None:
            warnings.warn(
                f"Skipped new video {video.url}: No subfolder for tag combination {','.join(video.tags)}"
            )
            continue
        export_md_folder(
            [video], os.path.join(folder, subfolder), taken_paths=taken_paths
        )


def map_youtube_tags_to_folder_tags(
    videos: Iterable[Video], subfolder_by_tags: dict[frozenset[str], str]
) -> Iterable[Video]:
    for video in videos:
        tag_found = False
        for tag_group, _ in subfolder_by_tags.items():
            if tag_group.issubset(video.tags):
                tag_found = True
                yield replace(video, tags=tag_group)
        if not tag_found:
            warnings.warn(f"Skipping video {video.url} ({','.join(video.tags)})")


def get_path(video: Video) -> str:
    if video.md_path is None:
        raise RuntimeError(f"Video with url {video.url} has no md path")
    return video.md_path


def diff_videos(
    reference: Sequence[Video], update: Sequence[Video]
) -> tuple[list[Video], list[Video], list[tuple[Video, Video]]]:
    reference_by_tagged_id = index_by_tagged_id(reference)
    update_by_tagged_id = index_by_tagged_id(update)
    new_videos = [
        video
        for y_id, video in update_by_tagged_id.items()
        if y_id not in reference_by_tagged_id
    ]
    deleted_videos = [
        video
        for y_id, video in reference_by_tagged_id.items()
        if y_id not in update_by_tagged_id
    ]
    video_changes = [
        (video, update_by_tagged_id[y_id])
        for y_id, video in reference_by_tagged_id.items()
        if y_id in update_by_tagged_id and diff_video(video, update_by_tagged_id[y_id])
    ]
    return new_videos, deleted_videos, video_changes


def diff_video(reference: Video, update: Video) -> dict[str, tuple[str, str]]:
    fields = ("title", "description", "created", "tags", "thumbnails")
    pairs = (
        (field, (getattr(reference, field), getattr(update, field))) for field in fields
    )
    return {field: (old, new) for field, (old, new) in pairs if old != new}


def index_by_tagged_id(
    videos: Iterable[Video],
) -> dict[tuple[str, frozenset[str]], Video]:
    return {(youtube_id(video), video.tags): video for video in videos}


def youtube_id(video: Video) -> str:
    ending = video.url.rsplit("/", 1)[-1]
    if "=" in ending:
        return ending.rsplit("=")[-1]
    return ending


def json_to_md(path: str, out: str, tags: frozenset[str]) -> None:
    videos = import_json(path)
    export_md_folder(
        [video for video in videos if frozenset(video.tags) & frozenset(tags)], out
    )


def import_json(path: str) -> list[Video]:
    with open(path, encoding="utf-8") as f:
        raw_videos = json.load(f)
    return [
        Video(tags=frozenset(raw_data.pop("tags")), **raw_data)
        for raw_data in raw_videos
    ]


def import_md_subfolders(
    folder: str, subfolder_by_tags: dict[frozenset[str], str]
) -> list[Video]:
    return sum(
        (
            import_md_folder(os.path.join(folder, subfolder), tags)
            for tags, subfolder in subfolder_by_tags.items()
        ),
        [],
    )


def import_md_folder(path: str, tags: frozenset[str] = frozenset()) -> list[Video]:
    files = glob.glob(os.path.join(path, "**", "*.md"), recursive=True)

    return list(
        check_duplicates(
            (v for v in (import_md(f, tags) for f in files) if v is not None),
            key=youtube_id,
            fmt=lambda video: video.md_path,
        )
    )


def check_duplicates(
    items: Iterable[Any], key: Callable[[Any], str], fmt: Callable[[Any], str] = repr
) -> Iterable[Any]:
    unique = set()
    duplicates = []

    for item in items:
        k = key(item)
        if k in unique:
            duplicates.append(item)
        unique.add(k)
        yield item
    if duplicates:
        warnings.warn(f"Found duplicates: {', '.join(fmt(i) for i in duplicates)}")


def import_md(path: str, tags: frozenset[str] = frozenset()) -> Optional[Video]:
    with open(path, encoding="utf-8") as f:
        content = f.read()
    parts = content.split("---")
    try:
        _, raw_frontmatter, content = parts
    except ValueError:
        if len(parts) < 3:
            raise ValueError(f"{path}: Missing frontmatter") from None
        raise ValueError(f"{path}: Too many --- separators in md") from None

    frontmatter = parse_simple_yml(raw_frontmatter)
    if frontmatter.pop("layout", None) not in (None, "Video"):
        return None
    return Video(**frontmatter, md_path=path, description=content.strip(), tags=tags)


def parse_simple_yml(body: str) -> dict[str, Any]:
    parsed: dict[str, Any] = {}
    indent = 0
    node_stack = [parsed]
    container_key = None
    for line in body.splitlines():
        if not line:
            continue
        original_len = len(line)
        line = line.lstrip(" ")
        current_indent = original_len - len(line)
        if current_indent % 2 != 0:
            raise ValueError("Indent must be a multiple of 2")
        current_indent //= 2
        if current_indent > indent:
            if container_key is None or current_indent - indent > 1:
                raise ValueError(f"Unexpected indent: {line}")
            new_node = {}
            node_stack[-1][container_key] = new_node
            node_stack.append(new_node)
            container_key = None
        if current_indent < indent:
            del node_stack[current_indent + 1 :]
        indent = current_indent
        try:
            val: Union[int, str]
            key, val = line.split(": ", 1)
            try:
                val = int(val)
            except ValueError:
                pass
            node_stack[-1][key] = val
        except ValueError:
            if not line.endswith(":"):
                raise ValueError(f"Unexpected line in prelude: {line}") from None
            container_key = line.strip(":")
            node_stack[-1][container_key] = None
    return parsed


def export_md_folder(
    videos: Iterable[Video], out: str, taken_paths: Optional[set[str]] = None
) -> None:
    if taken_paths is None:
        taken_paths = set()
    for video in videos:
        stem = os.path.join(out, slugify(video.title))
        path = stem + ".md"
        if path in taken_paths:
            for n in count(start=2):
                path = stem + f"-{n}.md"
                if not path in taken_paths:
                    break
        export_md(video, path)
        taken_paths.add(path)


def export_md(video: Video, path: str) -> None:
    prelude = f"""title: {video.title}
url: {video.url}
created: {video.created}"""
    if video.thumbnails:
        prelude += "\nthumbnails:\n" + dump_simple_yml(
            video.thumbnails, indent=1
        ).rstrip("\n")
    with open(path, "w", encoding="utf-8") as f:
        f.write(
            f"""---
{prelude}
---

{video.description}
"""
        )


def dump_simple_yml(data: dict[str, Any], indent: int = 0) -> str:
    out = ""
    for key, val in data.items():
        out += "  " * indent + f"{key}:"
        if isinstance(val, dict):
            out += "\n" + dump_simple_yml(val, indent + 1)
            continue
        out += f" {val}\n"
    return out


def slugify(title: str) -> str:
    return (
        title.lower()
        .replace(" ", "-")
        .replace(",", "-")
        .replace(".", "-")
        .replace(":", "-")
        .replace("/", "-")
        .replace("--", "-")
        .replace("ä", "ae")
        .replace("ö", "oe")
        .replace("ü", "ue")
        .strip("-")
    )


API = "https://www.googleapis.com/youtube/v3"


def list_youtube_videos(channel_id: str, api_key: str) -> list[dict[str, Any]]:
    url = f"{API}/search?channelId={channel_id}&part=snippet&order=date&maxResults=50&type=video"
    return combine_pages(url, api_key)


def get_youtube_video_details(
    youtube_ids: list[str], api_key: str
) -> list[dict[str, Any]]:
    urls = (
        f"{API}/videos?part=id,snippet&id={','.join(id_chunk)}&maxResults=50"
        for id_chunk in chunked(youtube_ids, 50)
    )
    return sum((combine_pages(url, api_key) for url in urls), [])


def chunked(items: Iterable[Any], n: int) -> Iterable[list[Any]]:
    it = iter(items)
    while True:
        chunk = [item for _, item in zip(range(n), it)]
        if not chunk:
            return
        yield chunk


def combine_pages(url: str, api_key: str) -> list[dict[str, Any]]:
    results = []
    next_token_query = ""
    headers = {"x-api-key": api_key}
    while True:
        result_pack = get(url + next_token_query + "&key=" + api_key, headers=headers)
        next_token = result_pack.get("nextPageToken")
        results += result_pack["items"]
        if not next_token:
            break
        next_token_query = f"&pageToken={next_token}"
    return results


def import_youtube_channels(channel_ids: list[str], api_key: str) -> list[Video]:
    return load_videos_from_dict(import_youtube_channels_raw(channel_ids, api_key))


def import_youtube_channels_raw(
    channel_ids: list[str], api_key: str
) -> list[dict[str, Any]]:
    return sum(
        (import_youtube_channel_raw(channel_id, api_key) for channel_id in channel_ids),
        [],
    )


def import_youtube_channel_raw(channel_id: str, api_key: str) -> list[dict[str, Any]]:
    raw_videos = list_youtube_videos(channel_id, api_key)
    video_ids = [raw_video["id"]["videoId"] for raw_video in raw_videos]
    return get_youtube_video_details(video_ids, api_key)


def load_videos_from_dict(
    raw_videos: Iterable[dict[str, Any]], channel_ids: frozenset[str] = frozenset()
) -> list[Video]:
    return [
        Video(
            title=raw_video["snippet"]["title"].strip(),
            url="https://youtu.be/" + raw_video["id"],
            description=raw_video["snippet"]["description"].strip(),
            created=raw_video["snippet"]["publishedAt"],
            thumbnails=raw_video["snippet"]["thumbnails"],
            tags=frozenset(raw_video["snippet"].get("tags", [])),
        )
        for raw_video in raw_videos
        if not channel_ids or raw_video["snippet"]["channelId"] in channel_ids
    ]


def load_videos_from_json(
    path: str, channel_ids: frozenset[str] = frozenset()
) -> list[Video]:
    with open(path, encoding="utf-8") as f:
        return load_videos_from_dict(json.load(f), channel_ids=channel_ids)


def cmd_sync_from_youtube(
    folder: Optional[str],
    config: Config,
    channel_ids: list[str],
    source: Optional[str] = None,
) -> None:
    if folder is None:
        folder = config.root
    if not channel_ids:
        channel_ids = config.channel_ids
    if source is not None:
        youtube_videos = load_videos_from_json(
            source, channel_ids=frozenset(channel_ids)
        )
    else:
        youtube_videos = import_youtube_channels(config.channel_ids, config.api_key)

    sync_from_youtube(
        youtube_videos,
        folder=folder,
        subfolder_by_tags=config.subfolder_by_tags,
    )


def cmd_list_youtube_videos(
    channel_ids: list[str], config: Config, source: Optional[str] = None
) -> None:
    if not channel_ids:
        channel_ids = config.channel_ids
    if source is not None:
        videos = load_videos_from_json(source, channel_ids=frozenset(channel_ids))
    else:
        videos = import_youtube_channels(channel_ids, api_key=config.api_key)
    print_videos(videos)


def cmd_dump_youtube_to_json(channel_ids: list[str], config: Config) -> None:
    if not channel_ids:
        channel_ids = config.channel_ids
    videos = import_youtube_channels_raw(channel_ids, api_key=config.api_key)
    json.dump(videos, sys.stdout, indent=2)


def load_config(path: str) -> Config:
    with open(path, encoding="utf-8") as f:
        return Config(**json.load(f))


def cmd_list_md(folder: str, config: Config) -> None:
    print_videos(import_md_subfolders(folder, config.subfolder_by_tags))


def print_videos(videos: Iterable[Video]) -> None:
    json.dump([asdict(v) for v in videos], sys.stdout, indent=2, default=list)


def cmd_video_details(youtube_ids: list[str], config: Config) -> None:
    json.dump(get_youtube_video_details(youtube_ids, config.api_key), sys.stdout)


def get(url: str, headers: dict[str, str]):
    request = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(request) as response:
        return json.loads(response.read())


@contextmanager
def catch_warnings():
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("always")
        yield
        for warning in w:
            # Note: the reason that "\n" are removed is so that
            # each warning gets logged on a single line.
            sys.stderr.write("⚠️ " + format(warning.message) + "\n")


if __name__ == "__main__":
    import argparse

    _default_config = os.environ.get("YOUTUBE2MD_CONFIG")
    if _default_config is None and os.path.isfile("config.json"):
        _default_config = "config.json"

    _config_arg: dict[str, Any] = dict(
        help="Config file",
        default=_default_config,
        required=_default_config is None,
        type=Config.load_from_file,
    )
    _source_arg = dict(help="Specify source json", default=None)

    _parser = argparse.ArgumentParser(description=os.path.basename(__file__))
    _subparsers = _parser.add_subparsers(
        dest="action",
        title="List of actions",
        description="For an overview of action specific parameters, "
        "use %(prog)s <ACTION> --help",
        help="Action help",
        metavar="<ACTION>",
    )
    _actions: list[
        tuple[Callable[..., Any], str, str, list[tuple[str, dict[str, Any]]]]
    ] = [
        (
            cmd_sync_from_youtube,
            "sync-youtube",
            "Sync from youtube to local md",
            [
                ("folder", dict(nargs="?", help="Root folder for md")),
                (
                    "--channel-id",
                    dict(
                        dest="channel_ids",
                        help="channel id",
                        action="append",
                        default=[],
                    ),
                ),
                ("--config", _config_arg),
                ("--source", _source_arg),
            ],
        ),
        (
            cmd_list_youtube_videos,
            "list-youtube",
            "List all videos for a given channel id",
            [
                ("channel_ids", dict(nargs="*", help="channel id")),
                ("--config", _config_arg),
                ("--source", _source_arg),
            ],
        ),
        (
            cmd_video_details,
            "video-details",
            "Fetch details for videos",
            [
                ("youtube_ids", dict(nargs="*", help="youtube id")),
                ("--config", _config_arg),
            ],
        ),
        (
            cmd_dump_youtube_to_json,
            "dump-youtube",
            "Fetch details for videos and dump json to stdout",
            [
                ("channel_ids", dict(nargs="*", help="channel id")),
                ("--config", _config_arg),
            ],
        ),
        (
            cmd_list_md,
            "list-md",
            "List local md files",
            [("folder", {}), ("--config", _config_arg)],
        ),
        (
            json_to_md,
            "import-json",
            "Import from json, write to md",
            [
                (
                    "-O",
                    dict(
                        dest="out", default=None, help="Output directory", required=True
                    ),
                ),
                (
                    "--tag",
                    dict(
                        default=None,
                        help="Tags to include",
                        action="append",
                        dest="tags",
                    ),
                ),
                ("video_file", {}),
            ],
        ),
    ]
    for action, action_name, action_help, action_args in _actions:
        _subparser = _subparsers.add_parser(action_name, help=action_help)
        for arg_name, arg_kwargs in action_args:
            _subparser.add_argument(arg_name, **arg_kwargs)

    _action_by_name: dict[str, Callable[..., Any]] = {
        action_name: action for action, action_name, *_ in _actions
    }
    _cmd_args = vars(_parser.parse_args())
    _action_name = _cmd_args.pop("action")
    try:
        with catch_warnings():
            _action_by_name[_action_name](**_cmd_args)
    except urllib.error.HTTPError as _e:
        _body = _e.fp.read().decode()
        sys.stderr.write(
            f"""💥 {_e.reason}
---
{_body}
"""
        )
        sys.exit(1)
