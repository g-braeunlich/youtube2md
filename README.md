# youtube2md

Script to fetch metadata from youtube and generate markdown files.

## Usage

Create a config file:

```json
{
  "root": "docs/gallery",
  "api_key": "...",
  "channel_ids": ["AIDSCmacSJc73nx2xh3780sx", "hd63L-d63y83mHGf63_dcuz3"],
  "subfolder_by_tags": [
    {
      "tags": ["tag_a"],
      "folder": "folder_a"
    },
    {
      "tags": ["tag_b1", "tag_b2"],
      "folder": "folder_b"
    }
  ]
}
```

Then sync:

```sh
./youtube2md.py sync-youtube
```

The script will fetch all metadata from all videos from the channels
specified in `channel_ids`.
According to the tags set on the videos, they will be placed in
subfolders of `root`: if all tags in a section of `subfolder_by_tags`
are set on a video, metadata to this video is written into the folder
specified in the same section.

Example for the above config:

| Video   | Tags      |
|---------|-----------|
| Video 1 | a, b1     |
| Video 2 | b1, b2    |
| Video 3 |           |
| Video 4 | a, b1, b2 |
| Video 5 | c         |

Then the output will be:

```
├── folder_a
│   ├── video-1.md
│   └── video-4.md
└── folder_b
    ├── video-2.md
    └── video-4.md
```

The `md` files will look like:

```md
---
title: Title of Video 1
url: https://youtu.be/...
created: 2021-02-11T22:04:49Z
---

Description of Video 1
```

## Using it in CI

The srcipt will look for the config at the path specified in the
`YOUTUBE2MD_CONFIG` env variable.
This allows for example in gitlab ci, to define a CI file variable `YOUTUBE2MD_CONFIG` containing the contents of the config.
