import unittest
from unittest import mock
from collections.abc import Sequence

import os
from typing import Any
from dataclasses import replace
from youtube2md import Video, get_path
import youtube2md


class TestUtils(unittest.TestCase):
    def test_chunked(self):
        lst = range(10)
        chunks = list(youtube2md.chunked(lst, n=3))
        self.assertEqual(chunks, [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]])


class TestSimpleYml(unittest.TestCase):
    def setUp(self):
        self.text = """title: Title
url: https://example.org/abc
created: 2020-04-24T14:15:01Z
thumbnails:
  default:
    url: https://i.example.org/vi/abc/default.jpg
    width: 120
    height: 90
  medium:
    url: https://i.example.org/vi/abc/mqdefault.jpg
    width: 320
    height: 180
  high:
    url: https://i.example.org/vi/abc/hqdefault.jpg
    width: 480
    height: 360
"""
        self.data = {
            "title": "Title",
            "url": "https://example.org/abc",
            "created": "2020-04-24T14:15:01Z",
            "thumbnails": {
                "default": {
                    "url": "https://i.example.org/vi/abc/default.jpg",
                    "width": 120,
                    "height": 90,
                },
                "medium": {
                    "url": "https://i.example.org/vi/abc/mqdefault.jpg",
                    "width": 320,
                    "height": 180,
                },
                "high": {
                    "url": "https://i.example.org/vi/abc/hqdefault.jpg",
                    "width": 480,
                    "height": 360,
                },
            },
        }

    def test_parse(self):
        parsed = youtube2md.parse_simple_yml(self.text)
        self.assertEqual(
            parsed,
            self.data,
        )

    def test_dump(self):
        text = youtube2md.dump_simple_yml(self.data)
        self.assertEqual(text, self.text)


class TestDiffVideo(unittest.TestCase):
    def test_diff_video(self) -> None:
        video = Video(
            url="",
            title="Title",
            description="Description",
            tags=frozenset({"Tag1", "Tag2"}),
        )
        with self.subTest(test="no diff for equal videos"):
            self.assertEqual(youtube2md.diff_video(video, video), {})
        video2 = Video(
            url="",
            title="Title",
            description="Description",
            tags=frozenset({"Tag2"}),
        )
        with self.subTest(test="diff for different tags"):
            self.assertEqual(
                youtube2md.diff_video(video, video2),
                {"tags": (frozenset({"Tag1", "Tag2"}), frozenset({"Tag2"}))},
            )


class TestDiffVideos(unittest.TestCase):
    def setUp(self) -> None:
        self.video = Video(
            url="https://www.youtube.com/watch?v=0hTfdlcmier",
            title="Title",
            description="Description",
            tags=frozenset({"Tag1", "Tag2"}),
        )
        self.video2 = Video(
            url="https://youtu.be/00000000000",
            title="Title2",
            description="Description2",
            tags=frozenset({"Tag3", "Tag4"}),
        )
        self.video2_alt = Video(
            url="https://www.youtube.com/watch?v=00000000000",
            title="Title2",
            description="Description2",
            tags=frozenset({"Tag3", "Tag4"}),
        )
        self.video2_mod = Video(
            url="https://youtu.be/00000000000",
            title="Title2_mod",
            description="Description2",
            tags=frozenset({"Tag3", "Tag4"}),
        )

    def test_no_diff_for_equal_videos(self) -> None:
        with self.subTest(test="equal lists"):
            self.assertEqual(
                youtube2md.diff_videos(
                    [self.video, self.video2], [self.video, self.video2]
                ),
                ([], [], []),
            )
        with self.subTest(test="essential equal lists"):
            self.assertEqual(
                youtube2md.diff_videos(
                    [self.video, self.video2], [self.video, self.video2_alt]
                ),
                ([], [], []),
            )

    def test_new_video(self) -> None:
        self.assertEqual(
            youtube2md.diff_videos([self.video], [self.video, self.video2]),
            ([self.video2], [], []),
        )

    def test_deleted_video(self) -> None:
        self.assertEqual(
            youtube2md.diff_videos([self.video, self.video2], [self.video]),
            ([], [self.video2], []),
        )

    def test_changed_video(self) -> None:
        self.assertEqual(
            youtube2md.diff_videos(
                [self.video, self.video2], [self.video, self.video2_mod]
            ),
            ([], [], [(self.video2, self.video2_mod)]),
        )


class TestSyncWithoutChanges(unittest.TestCase):
    def setUp(self) -> None:
        self.channel_ids = ["Ch_A", "Ch_B"]
        self.folder = "/folder"
        self.api_key = "API"
        self.subfolder_by_tags = {
            frozenset({"tag_a"}): "sub_a",
            frozenset({"tag_b1", "tag_b2"}): "sub_b",
        }
        self.config = youtube2md.Config(
            subfolder_by_tags=self.subfolder_by_tags,
            root=self.folder,
            channel_ids=self.channel_ids,
            api_key=self.api_key,
        )
        self.videos_md = [
            Video(
                url="https://youtu.be/00000000000",
                title="Title",
                description="Description",
                tags=frozenset({"tag_a"}),
                md_path="/folder/sub_a/title.md",
            ),
            Video(
                url="https://youtu.be/00000000002",
                title="Title 2",
                description="Description 2",
                tags=frozenset({"tag_b1", "tag_b2"}),
                md_path="/folder/sub_b/title-2.md",
            ),
        ]
        self.videos_yt = {
            "Ch_A": [
                Video(
                    url="https://youtu.be/00000000000",
                    title="Title",
                    description="Description",
                    tags=frozenset({"tag_a"}),
                )
            ],
            "Ch_B": [
                Video(
                    url="https://youtu.be/00000000002",
                    title="Title 2",
                    description="Description 2",
                    tags=frozenset({"tag_b1", "tag_b2"}),
                )
            ],
        }
        self.expected_calls_import_md = [
            mock.call("/folder/sub_a", frozenset({"tag_a"})),
            mock.call("/folder/sub_b", frozenset({"tag_b1", "tag_b2"})),
        ]
        self.expected_calls_import_yt = [
            mock.call(["Ch_A", "Ch_B"], self.api_key),
        ]
        self.expected_calls_unlink: list[Any] = []
        self.expected_calls_export_md: list[Any] = []

    def test_sync(self) -> None:
        mock_import_md_folder = mock.Mock(
            side_effect=make_mock_import_md(
                self.videos_md,
                folders=tuple(
                    os.path.join(self.folder, f)
                    for f in self.subfolder_by_tags.values()
                ),
            )
        )
        mock_import_yt = mock.Mock(side_effect=make_mock_import_youtube(self.videos_yt))
        mock_unlink = mock.Mock()
        mock_export_md = mock.Mock()

        with mock.patch(
            "youtube2md.import_md_folder", mock_import_md_folder
        ), mock.patch("youtube2md.import_youtube_channels", mock_import_yt), mock.patch(
            "youtube2md.os.unlink", mock_unlink
        ), mock.patch(
            "youtube2md.export_md", mock_export_md
        ):
            youtube2md.cmd_sync_from_youtube(
                channel_ids=[], folder=None, config=self.config
            )
        assert_equal_calls(mock_import_md_folder, self.expected_calls_import_md)
        assert_equal_calls(mock_import_yt, self.expected_calls_import_yt)
        assert_equal_calls(mock_unlink, self.expected_calls_unlink)
        assert_equal_calls(mock_export_md, self.expected_calls_export_md)


def assert_equal_calls(mock_obj: Any, calls):
    mock_obj.assert_has_calls(calls, any_order=True)
    if mock_obj.call_count != len(calls):
        raise AssertionError(
            f"Mock has more calls than expected: {repr(mock_obj.call_args_list)}"
        )


class TestSyncWithChanges(TestSyncWithoutChanges):
    def setUp(self) -> None:
        super().setUp()
        self.videos_md = [
            Video(
                url="https://youtu.be/00000000000",
                title="Title",
                description="Description",
                md_path="/folder/sub_a/title.md",
                tags=frozenset({"tag_a"}),
            ),
            Video(
                url="https://youtu.be/00000000002",
                title="Title 2",
                description="Description 2",
                md_path="/folder/sub_b/title-2.md",
                tags=frozenset({"tag_b1", "tag_b2"}),
            ),
        ]
        new_video = Video(
            url="https://youtu.be/00000000003",
            title="Title New",
            description="Description New",
            tags=frozenset({"tag_a"}),
        )
        changed_video = Video(
            url="https://youtu.be/00000000002",
            title="Title 2",
            description="Description 2 changed",
            tags=frozenset({"tag_b1", "tag_b2"}),
        )
        self.videos_yt = {
            "Ch_A": [new_video],
            "Ch_B": [
                changed_video,
                Video(
                    url="https://youtu.be/00000000003",
                    title="Untagged",
                    description="Untagged video",
                ),
            ],
        }
        self.expected_calls_unlink: list[Any] = [mock.call("/folder/sub_a/title.md")]
        self.expected_calls_export_md: list[Any] = [
            mock.call(new_video, "/folder/sub_a/title-new.md"),
            mock.call(changed_video, "/folder/sub_b/title-2.md"),
        ]

    def test_sync(self) -> None:
        with self.assertWarns(UserWarning):
            super().test_sync()


class TestSyncWithDuplicatedVideos(TestSyncWithoutChanges):
    def setUp(self) -> None:
        super().setUp()
        self.videos_md = [
            Video(
                url="https://youtu.be/00000000000",
                title="Title",
                description="Description",
                md_path="/folder/sub_a/title.md",
                tags=frozenset({"tag_a"}),
            ),
            Video(
                url="https://youtu.be/00000000000",
                title="Title Wrong",
                description="Description Wrong",
                md_path="/folder/sub_b/title.md",
                tags=frozenset({"tag_b1", "tag_b2"}),
            ),
            Video(
                url="https://youtu.be/00000000001",
                title="Title 1",
                description="Description 1",
                md_path="/folder/sub_a/title-1.md",
                tags=frozenset({"tag_a"}),
            ),
            Video(
                url="https://youtu.be/00000000001",
                title="Title 1",
                description="Description 1",
                md_path="/folder/sub_b/wrong.md",
                tags=frozenset({"tag_b1", "tag_b2"}),
            ),
        ]
        youtube_video = Video(
            url="https://youtu.be/00000000000",
            title="Title",
            description="Description",
            tags=frozenset({"tag_b1", "tag_b2", "tag_a"}),
        )
        youtube_video_1 = Video(
            url="https://youtu.be/00000000001",
            title="Title 1",
            description="Description 1",
            tags=frozenset({"tag_a", "unimportant_tag"}),
        )
        self.videos_yt = {"Ch_A": [youtube_video, youtube_video_1], "Ch_B": []}
        self.expected_calls_unlink: list[Any] = [mock.call("/folder/sub_b/wrong.md")]
        self.expected_calls_export_md: list[Any] = [
            mock.call(
                replace(youtube_video, tags=frozenset({"tag_b1", "tag_b2"})),
                "/folder/sub_b/title.md",
            )
        ]


class TestSyncWithNameCollision(TestSyncWithoutChanges):
    def setUp(self) -> None:
        super().setUp()
        self.videos_md = []
        youtube_video = Video(
            url="https://youtu.be/00000000000",
            title="Colliding Title",
            description="Description",
            tags=frozenset({"tag_a"}),
        )
        youtube_video_1 = Video(
            url="https://youtu.be/00000000001",
            title="Colliding Title",
            description="Description 1",
            tags=frozenset({"tag_a", "unimportant_tag"}),
        )
        self.videos_yt = {"Ch_A": [youtube_video, youtube_video_1], "Ch_B": []}
        self.expected_calls_export_md: list[Any] = [
            mock.call(
                youtube_video,
                "/folder/sub_a/colliding-title.md",
            ),
            mock.call(
                replace(youtube_video_1, tags=frozenset({"tag_a"})),
                "/folder/sub_a/colliding-title-2.md",
            ),
        ]


def make_mock_import_md(videos: list[Video], folders: Sequence[str] = ()):
    videos_by_folder: dict[str, list[Video]] = {folder: [] for folder in folders}
    for v in videos:
        videos_by_folder.setdefault(os.path.dirname(get_path(v)), []).append(v)

    def mock_import_md(path: str, tags: Any = frozenset()) -> list[Video]:
        assert isinstance(tags, frozenset)
        try:
            return videos_by_folder[path]
        except KeyError as e:
            raise FileNotFoundError(*e.args) from None

    return mock_import_md


def make_mock_import_youtube(videos_by_channel: dict[str, list[Video]]):
    def mock_import_yt(channel_ids: list[str], api_key: str) -> list[Video]:
        assert isinstance(api_key, str)
        return sum((videos_by_channel[channel_id] for channel_id in channel_ids), [])

    return mock_import_yt
